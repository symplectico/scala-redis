package scadis

import cats.effect.{Deferred, IO}
import cats.effect.ExitCode
import weaver.{Expect, Expectations, SimpleIOSuite}
import com.comcast.ip4s.Port

import scala.concurrent.duration.*
import scadis.protocol.ScadisProtocolType
import scadis.protocol.ScadisProtocolType.{BulkString, Nil, SimpleString}
import scadis.server.{Constants, Server}
import scadis.TestClient

object ServerTestSuite extends SimpleIOSuite {

  def generateServerTest(
      testInput: List[String],
      testResult: List[ScadisProtocolType]
  ): IO[Expectations] = {
    val port   = Port.fromInt(5555).get
    val server = Server.start[IO](port).compile.drain

    for {
      serverFib <- server.start
      _         <- IO.sleep(1.seconds)
      client    <- TestClient.make(testInput)
      result    <- client.start
      _         <- serverFib.cancel
    } yield expect(result == testResult)
  }

  test("test set and get") {
    val testInput = List("GET ASDF", "SET ASDF 1", "GET ASDF", "SET ASDF 2", "GET ASDF")
    val testResult = List(
      Nil,
      SimpleString(Constants.OK),
      BulkString("1"),
      SimpleString(Constants.OK),
      BulkString("2")
    )
    generateServerTest(testInput, testResult)

  }
}
