package scadis

import cats.effect.{IO, Ref}
import cats.effect.implicits.*
import cats.effect.kernel.Resource
import cats.effect.std.Console
import com.comcast.ip4s.{IpAddress, Port, SocketAddress}
import fs2.Stream
import fs2.concurrent.SignallingRef
import fs2.interop.scodec.{StreamDecoder, StreamEncoder}
import fs2.io.net.{Network, Socket}
import scadis.protocol.{Codecs, ScadisProtocolType}
import scadis.protocol.ScadisProtocolType.{BulkString, SArray}

import scala.concurrent.duration.*

case class TestClient(
    input: List[String],
    completedSignal: SignallingRef[IO, Boolean],
    output: Ref[IO, List[ScadisProtocolType]]
) {

  def clientSocket: Resource[IO, Socket[IO]] = Network[IO].client(
    SocketAddress(
      IpAddress.fromString("127.0.0.1").get,
      Port.fromInt(5555).get
    )
  )

  def processIncoming(socket: Socket[IO]): Stream[IO, Unit] = socket.reads
    .through(StreamDecoder.many(Codecs.redisType).toPipeByte)
    .head
    .evalTap(response => output.update(output => response :: output))
    .drain

  def processOutgoing(line: String, socket: Socket[IO]): Stream[IO, Unit] = Stream
    .emit(line)
    .map(s => new SArray(s.split("\\s+").toVector.map(BulkString.apply)))
    .through(StreamEncoder.many(Codecs.array).toPipeByte[IO])
    .through(socket.writes)
    .drain

  def start: IO[List[ScadisProtocolType]] = {
    (Stream
      .resource(clientSocket)
      .flatMap { socket =>
        Stream.emits(input).flatMap { line =>
          processIncoming(socket)
            .concurrently(processOutgoing(line, socket))
        } ++ Stream.eval(completedSignal.set(true))
      }
      .interruptWhen(completedSignal)
      >> Stream.eval(output.get.map(_.reverse)).flatMap(Stream.emits(_))).compile.toList
  }
}

object TestClient {
  def make(input: List[String]): IO[TestClient] = for {
    signal <- SignallingRef[IO, Boolean](false)
    ref    <- Ref.of[IO, List[ScadisProtocolType]](List())
  } yield TestClient(input, signal, ref)
}
