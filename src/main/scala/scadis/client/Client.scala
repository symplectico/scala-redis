package scadis.client

import cats.implicits.*
import cats.effect.Concurrent
import cats.effect.std.Console
import cats.effect.implicits.*
import fs2.{INothing, RaiseThrowable, Stream}
import fs2.io.net.{Network, Socket}
import fs2.interop.scodec.{StreamEncoder, StreamDecoder}
import com.comcast.ip4s.SocketAddress
import com.comcast.ip4s.IpAddress
import com.comcast.ip4s.Port
import scadis.protocol.ScadisProtocolType
import scadis.protocol.ScadisProtocolType.*
import scadis.protocol.Codecs

object Client {
  def start[F[_]: Concurrent: Console: Network]: Stream[F, Unit] = connect

  def connect[F[_]: Concurrent: Console: Network]: Stream[F, Unit] = {
    Stream.exec(Console[F].println(s"Connecting to server on port 5555")) ++
      Stream
        .resource(
          Network[F].client(
            SocketAddress(
              IpAddress.fromString("127.0.0.1").get,
              Port.fromInt(5555).get
            )
          )
        )
        .flatMap { socket =>
          Stream.exec(Console[F].println("Connected!")) ++
            processIncoming(socket).concurrently(processOutgoing(socket))
        }
  }

  def processIncoming[F[_]: Concurrent: Console](
      socket: Socket[F]
  ): Stream[F, Unit] = {
    socket.reads
      .through(StreamDecoder.many(Codecs.redisType).toPipeByte)
      .foreach(Console[F].println(_))
  }

  def processOutgoing[F[_]: Console: Concurrent](
      socket: Socket[F]
  ): Stream[F, Unit] = {
    Stream
      .repeatEval(Console[F].readLine)
      .map(s => new SArray(s.split("\\s+").toVector.map(BulkString(_))))
      .through(StreamEncoder.many(Codecs.array).toPipeByte)
      .through(socket.writes)
      .drain
  }
}
