package scadis.client

import cats.effect.IOApp
import cats.effect.ExitCode
import cats.effect.IO
import scadis.server.ListCommands.Side

object ClientApp extends IOApp {
  override def run(args: List[String]): IO[ExitCode] =
    Client.start[IO].compile.drain.as(ExitCode.Success)
}
