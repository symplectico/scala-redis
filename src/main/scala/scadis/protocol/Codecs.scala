package scadis.protocol

import scodec.Attempt.{Failure, Successful}
import scodec.{Attempt, Codec, DecodeResult, Err, Iso, SizeBound}
import scodec.bits.{BitVector, ByteVector, hex}
import scodec.codecs.*

object Codecs {

  import ScadisProtocolType.*

  private val crlf: ByteVector = ByteVector('\r', '\n')

  private val crlfString: Codec[String] = filtered(
    utf8,
    new Codec[BitVector] {

      override def sizeBound: SizeBound = SizeBound.unknown

      override def encode(bits: BitVector): Attempt[BitVector] =
        Attempt.successful(bits ++ crlf.bits)

      override def decode(bits: BitVector): Attempt[DecodeResult[BitVector]] =
        bits.bytes.indexOfSlice(crlf) match {
          case -1 =>
            Attempt.failure(
              Err.InsufficientBits(-1, -1, List())
            )
          case i =>
            Attempt.successful(
              DecodeResult(bits.take(i * 8L), bits.drop((i + 2) * 8L))
            )
        }
    }
  )

  private val crlfByteNumber: Codec[Int] = crlfString.xmap(
    s => Integer.parseInt(s) + crlf.size.toInt,
    i => (i - crlf.size).toString
  )

  private val crlfElementNumber: Codec[Int] =
    crlfString.xmap(s => Integer.parseInt(s), i => i.toString)

  val nil: Codec[Nil.type] = constant(ByteVector("$-1\r\n".map(_.toByte)))
    .xmap(
      _ => Nil,
      _ => ()
    )

  val simpleString: Codec[SimpleString] =
    (constant('+') ~> crlfString).as[SimpleString]

  val error: Codec[SError] = (constant('-') ~> crlfString).as[SError]

  val integer: Codec[SInteger] = (constant(':') ~> crlfString)
    .as[SimpleString]
    .xmap(
      ss => SInteger(Integer.parseInt(ss.string)),
      ri => SimpleString(ri.int.toString)
    )

  val bulkString: Codec[BulkString] =
    (constant('$') ~> variableSizeBytes(crlfByteNumber, crlfString))
      .as[BulkString]

  val array: Codec[SArray] = (constant('*') ~>
    vectorOfN(
      crlfElementNumber,
      choice[ScadisProtocolType](
        nil.upcast[ScadisProtocolType],
        simpleString.upcast[ScadisProtocolType],
        error.upcast[ScadisProtocolType],
        integer.upcast[ScadisProtocolType],
        bulkString.upcast[ScadisProtocolType]
      )
    )).xmap(vec => SArray(vec), rarr => rarr.vals)

  val redisType: Codec[ScadisProtocolType] =
    choice[ScadisProtocolType](
      array.upcast[ScadisProtocolType],
      nil.upcast[ScadisProtocolType],
      simpleString.upcast[ScadisProtocolType],
      error.upcast[ScadisProtocolType],
      integer.upcast[ScadisProtocolType],
      bulkString.upcast[ScadisProtocolType]
    )

  def prettyPrint(bytes: ByteVector) = bytes.toSeq
    .map(_.toChar.toString)
    .map {
      case "\r" => "\\r"
      case "\n" => "\\n"
      case s    => s
    }
    .mkString
}
