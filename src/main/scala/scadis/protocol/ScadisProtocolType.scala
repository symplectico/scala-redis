package scadis.protocol

enum ScadisProtocolType {
  case Nil
  case SimpleString(string: String)
  case SError(error: String)
  case SInteger(int: Integer)
  case BulkString(string: String)
  case SArray(vals: Vector[ScadisProtocolType])
}
