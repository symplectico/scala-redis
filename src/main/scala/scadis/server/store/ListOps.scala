package scadis.server.store

import scala.util.{Success, Try}

import cats.Monad
import cats.implicits.*
import cats.syntax.all.*

import cats.effect.std.Console

import scadis.server.Constants
import scadis.server.ScadisDataType
import scadis.server.ScadisDataType.*
import scadis.server.store.StoreOperationResult

trait ListOps[F[_], K, V] {
  def lindex(key: K, index: Int): F[StoreOperationResult[Option[V]]]

  def linsert(key: K, insertBefore: Boolean, pivot: V, element: V): F[StoreOperationResult[Int]]

  def llen(key: K): F[StoreOperationResult[Int]]

  def lmove(
      source: K,
      destination: K,
      fromLeft: Boolean,
      toLeft: Boolean
  ): F[StoreOperationResult[Option[V]]]

  def lmpop(
      numkeys: Int,
      keys: IterableOnce[K],
      fromLeft: Boolean,
      count: Option[Int]
  ): F[StoreOperationResult[V]]

  def lpop(key: K, count: Option[Int]): F[StoreOperationResult[V]]

  def lpos(key: K, element: V): F[StoreOperationResult[Option[Int]]] // TODO rank, count, maxlen

  def lpush(key: K, elements: IterableOnce[V]): F[StoreOperationResult[Int]]

  def lpushx(key: K, elements: IterableOnce[V]): F[StoreOperationResult[Int]]

  def lrange(key: K, start: Int, stop: Int): F[StoreOperationResult[V]]

  def lrem(key: K, count: Int, element: V): F[StoreOperationResult[Int]]

  def lset(key: K, index: Int, element: V): F[StoreOperationResult[Unit]]

  def ltrim(key: K, start: Int, stop: Int): F[StoreOperationResult[Unit]]

  def rpop(key: K, count: Option[Int]): F[StoreOperationResult[Option[V]]]

  def rpush(key: K, elements: IterableOnce[V]): F[StoreOperationResult[Int]]

  def rpushx(key: K, elements: IterableOnce[V]): F[StoreOperationResult[Int]]
}

trait ListOpsImpl[F[_]: Monad, K]
    extends RefStore[F, K, ScadisDataType]
    with ListOps[F, K, ScadisDataType] {
  override def lindex(key: K, index: Int): F[StoreOperationResult[Option[ScadisDataType]]] =
    state.get.map { valueStore =>
      valueStore
        .get(key)
        .map {
          case ScadisList(values) => {
            val normalizedIndex = if index >= 0 then index else values.size + index
            if normalizedIndex >= 0 && normalizedIndex < values.size then
              Right(Some(ScadisString(values(normalizedIndex))))
            else Right(None)
          }
          case _ => Left(StoreError(Constants.WRONGTYPE))
        }
        .getOrElse(Right(None))
    }

  override def linsert(
      key: K,
      insertBefore: Boolean,
      pivot: ScadisDataType,
      element: ScadisDataType
  ): F[StoreOperationResult[Int]] = state.modify { valueStore =>
    (pivot, element) match
      case (ScadisString(pivotString), ScadisString(elementString)) => {
        valueStore
          .get(key)
          .map {
            case ScadisList(values) => {
              val index = values.indexOf(pivotString)
              if index == -1 then (valueStore, Either.right[StoreError, Int](-1))
              else {
                if insertBefore then
                  (
                    valueStore.updated(
                      key,
                      ScadisList(
                        values.slice(0, index) ++ Vector(elementString) ++ values
                          .slice(index, values.size)
                      )
                    ),
                    Either.right[StoreError, Int](values.size + 1)
                  )
                else
                  (
                    valueStore.updated(key, ScadisList(values)),
                    Either.right[StoreError, Int](values.size + 1)
                  )
              }
            }
            case _ => (valueStore, Either.left[StoreError, Int](StoreError(Constants.WRONGTYPE)))
          }
          .getOrElse((valueStore, Either.right[StoreError, Int](0)))
      }
      case _ => (valueStore, Either.left[StoreError, Int](StoreError(Constants.WRONGTYPE)))
  }

  override def llen(key: K): F[StoreOperationResult[Int]] = state.get.map { valueStore =>
    valueStore.get(key) match
      case Some(value) =>
        value match
          case ScadisList(lst) => Either.right(lst.size)
          case _               => Either.left(StoreError(Constants.WRONGTYPE))
      case None => Either.right(0)
  }

  override def lmove(
      source: K,
      destination: K,
      fromLeft: Boolean,
      toLeft: Boolean
  ): F[StoreOperationResult[Option[ScadisDataType]]] = state.modify { valueStore =>
    valueStore.get(source) match
      case Some(ScadisList(srcLst)) => {
        valueStore.get(destination) match
          case Some(ScadisList(destLst)) => {
            (fromLeft, toLeft) match
              case (true, true) =>
                (
                  if source != destination then
                    valueStore
                      .updated(source, ScadisList(srcLst.tail))
                      .updated(destination, ScadisList(srcLst.head +: destLst))
                  else valueStore,
                  Right(Some(ScadisString(srcLst.head)))
                )
              case (true, false) =>
                (
                  if source != destination then
                    valueStore
                      .updated(source, ScadisList(srcLst.tail))
                      .updated(destination, ScadisList(destLst :+ srcLst.head))
                  else valueStore.updated(source, ScadisList(srcLst.tail :+ srcLst.head)),
                  Right(Some(ScadisString(srcLst.head)))
                )
              case (false, true) =>
                (
                  if source != destination then
                    valueStore
                      .updated(source, ScadisList(srcLst.dropRight(1)))
                      .updated(destination, ScadisList(srcLst.last +: destLst))
                  else valueStore.updated(source, ScadisList(srcLst.dropRight(1) :+ srcLst.last)),
                  Right(Some(ScadisString(srcLst.last)))
                )
              case (false, false) =>
                (
                  if source != destination then
                    valueStore
                      .updated(source, ScadisList(srcLst.dropRight(1)))
                      .updated(destination, ScadisList(destLst :+ srcLst.last))
                  else valueStore,
                  Right(Some(ScadisString(srcLst.last)))
                )
          }
          case Some(_) => (valueStore, Left(StoreError(Constants.WRONGTYPE)))
          case None => {
            if fromLeft then (valueStore, Right(Some(ScadisString(srcLst.head))))
            else (valueStore, Right(Some(ScadisString(srcLst.last))))
          }
      }
      case Some(_) => (valueStore, Left(StoreError(Constants.WRONGTYPE)))
      case None    => (valueStore, Right(None))
  }

  override def lmpop(
      numkeys: Int,
      keys: IterableOnce[K],
      fromLeft: Boolean,
      count: Option[Int]
  ): F[StoreOperationResult[ScadisDataType]] = ???

  override def lpop(key: K, count: Option[Int]): F[StoreOperationResult[ScadisDataType]] = ???

  override def lpos(key: K, element: ScadisDataType): F[StoreOperationResult[Option[Int]]] = ???

  override def lpush(key: K, elements: IterableOnce[ScadisDataType]): F[StoreOperationResult[Int]] =
    state.modify { valueStore =>
      valueStore.get(key) match
        case Some(ScadisList(lst)) => {
          val newLst = elements.iterator.toVector.reverse
            .asInstanceOf[Vector[ScadisString]]
            .map(_.string) ++ lst
          (valueStore.updated(key, ScadisList(newLst)), Right(newLst.size))
        }
        case Some(_) => (valueStore, Left(StoreError(Constants.WRONGTYPE)))
        case None => {
          val newLst =
            elements.iterator.toVector.reverse.asInstanceOf[Vector[ScadisString]].map(_.string)
          println(newLst)
          (valueStore.updated(key, ScadisList(newLst)), Right(newLst.size))
        }
    }

  override def lpushx(
      key: K,
      elements: IterableOnce[ScadisDataType]
  ): F[StoreOperationResult[Int]] =
    ???

  override def lrange(key: K, start: Int, stop: Int): F[StoreOperationResult[ScadisDataType]] = {
    state.get.map { map =>
      map.get(key) match {
        case Some(value) =>
          value match {
            case ScadisList(lst) => {
              val len             = lst.length
              val normalizedStart = if start >= 0 then start else len - start
              val normalizedEnd   = if stop >= 0 then stop else len - stop
              if normalizedStart >= normalizedEnd then Right(ScadisList(Vector()))
              else
                Right(
                  ScadisList(
                    lst.slice(
                      Math.min(normalizedStart, len - 1),
                      Math.min(normalizedEnd, len - 1)
                    )
                  )
                )
            }
            case _ => Left(StoreError(Constants.WRONGTYPE))
          }
        case None => Right(ScadisList(Vector()))
      }
    }
  }

  override def lrem(key: K, count: Int, element: ScadisDataType): F[StoreOperationResult[Int]] = ???

  override def lset(key: K, index: Int, element: ScadisDataType): F[StoreOperationResult[Unit]] =
    ???

  override def ltrim(key: K, start: Int, stop: Int): F[StoreOperationResult[Unit]] = ???

  override def rpop(key: K, count: Option[Int]): F[StoreOperationResult[Option[ScadisDataType]]] =
    ???

  override def rpush(key: K, elements: IterableOnce[ScadisDataType]): F[StoreOperationResult[Int]] =
    ???

  override def rpushx(
      key: K,
      elements: IterableOnce[ScadisDataType]
  ): F[StoreOperationResult[Int]] =
    ???
}
