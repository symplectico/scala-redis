package scadis.server.store

import cats.Monad
import cats.syntax.all.*

import scadis.server.Constants
import scadis.server.ScadisDataType
import scadis.server.ScadisDataType.*
import scadis.server.store.RefStore

trait GenericOps[F[_], K, V] {
  def copy(sourceKey: K, destinationKey: K): F[Boolean]

  def del(keys: IterableOnce[K]): F[Int]

  def exists(keys: IterableOnce[K]): F[Int]

  def typeOp(key: K): F[String]
}

trait GenericOpsImpl[F[_]: Monad, K]
    extends RefStore[F, K, ScadisDataType]
    with GenericOps[F, K, ScadisDataType] {

  override def copy(sourceKey: K, destinationKey: K): F[Boolean] = state.modify { map =>
    map.get(destinationKey) match
      case Some(value) => (map.updated(destinationKey, value), true)
      case None        => (map, false)
  }

  override def del(keys: IterableOnce[K]): F[Int] = state.modify { map =>
    keys.iterator.foldLeft((map, 0)) { (accu, key) =>
      (accu._1 - key, accu._2 + (if accu._1.contains(key) then 1 else 0))
    }
  }

  override def exists(keys: IterableOnce[K]): F[Int] = state.get.map { map =>
    keys.iterator.foldLeft(0) { (cnt, key) =>
      cnt + (if map.contains(key) then 1 else 0)
    }
  }

  override def typeOp(key: K): F[String] = state.get.map { map =>
    map
      .get(key)
      .map {
        case ScadisString(_)  => Constants.TYPE_STRING
        case ScadisList(_)    => Constants.TYPE_LIST
        case ScadisHashMap(_) => Constants.TYPE_HASH_MAP
        case ScadisSet(_)     => Constants.TYPE_SET
      }
      .getOrElse(Constants.TYPE_NONE)
  }
}
