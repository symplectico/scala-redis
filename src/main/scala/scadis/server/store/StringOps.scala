package scadis.server.store

import scala.util.{Success, Try}

import cats.Monad
import cats.syntax.all.*

import cats.effect.std.Console

import scadis.server.Constants
import scadis.server.ScadisDataType
import scadis.server.ScadisDataType.*
import scadis.server.store.StoreOperationResult

trait StringOps[F[_], K, V] {
  def append(key: K, value: V): F[StoreOperationResult[Int]]

  def decr(key: K): F[StoreOperationResult[Int]]

  def decrby(key: K, decrement: Int): F[StoreOperationResult[Int]]

  def get(key: K): F[StoreOperationResult[Option[V]]]

  def getdel(key: K): F[StoreOperationResult[Option[V]]]

  def getrange(key: K, start: Int, end: Int): F[StoreOperationResult[V]]

  def getset(key: K, value: V): F[Option[V]]

  def incr(key: K): F[StoreOperationResult[Int]]

  def incrby(key: K, increment: Int): F[StoreOperationResult[Int]]

  def mget(keys: IterableOnce[K]): F[IterableOnce[V]]

  def mset(keyVals: IterableOnce[(K, V)]): F[Unit]

  def set(key: K, value: V): F[Unit]

  def strlen(key: K): F[StoreOperationResult[Int]]
}

trait StringOpsImpl[F[_]: Console: Monad, K]
    extends RefStore[F, K, ScadisDataType]
    with StringOps[F, K, ScadisDataType] {

  override def set(key: K, value: ScadisDataType): F[Unit] = state.update(
    _.updated(key, value)
  ) *> Console[F].println(s"setting $key to $value")

  override def get(key: K): F[StoreOperationResult[Option[ScadisDataType]]] = state.get
    .map(_.get(key) match {
      case Some(ScadisString(s)) => Right(Some(ScadisString(s)))
      case Some(_)               => Left(StoreError(Constants.WRONGTYPE))
      case _                     => Right(None)
    })

  override def getdel(key: K): F[StoreOperationResult[Option[ScadisDataType]]] = state.modify {
    map =>
      (
        map.removed(key),
        map.get(key) match {
          case Some(ScadisString(s)) => Right(Some(ScadisString(s)))
          case Some(_)               => Left(StoreError(Constants.WRONGTYPE))
          case _                     => Right(None)
        }
      )
  }

  override def getset(key: K, value: ScadisDataType): F[Option[ScadisDataType]] = state.modify {
    map =>
      (
        map.updated(key, value),
        map.get(key).flatMap {
          case ScadisString(s) => Some(ScadisString(s))
          case _               => None
        }
      )
  }

  override def mget(keys: IterableOnce[K]): F[IterableOnce[ScadisDataType]] = state.get.map { map =>
    keys.iterator.foldLeft(List.empty[ScadisString]) { (accu, key) =>
      map.get(key) match {
        case Some(ScadisString(s)) => ScadisString(s) :: accu
        case _                     => accu
      }
    }
  }

  override def mset(keyVals: IterableOnce[(K, ScadisDataType)]): F[Unit] = state.update { map =>
    keyVals.iterator.foldLeft(map) { (accu, keyVal) =>
      accu.updated(keyVal._1, keyVal._2)
    }
  }

  override def append(key: K, appendage: ScadisDataType): F[StoreOperationResult[Int]] =
    state.modify { map =>
      map.get(key) match {
        case Some(value) => {
          (value, appendage) match {
            case (ScadisString(s), ScadisString(sa)) => {
              val newValue = s + sa
              (map.updated(key, ScadisString(newValue)), Right(newValue.length))
            }
            case _ => (map, Left(StoreError("")))
          }
        }
        case _ =>
          appendage match
            case app @ ScadisString(sa) => (map.updated(key, app), Right(sa.length))
            case _                      => (map, Left(StoreError("")))
      }
    }

  override def decr(key: K): F[StoreOperationResult[Int]] = decrby(key, 1)

  override def decrby(key: K, decrement: Int): F[StoreOperationResult[Int]] =
    incrby(key, -decrement)

  override def getrange(key: K, start: Int, end: Int): F[StoreOperationResult[ScadisDataType]] = {
    state.get.map { map =>
      map.get(key) match {
        case Some(value) =>
          value match {
            case ScadisString(s) => {
              val len             = s.length
              val normalizedStart = if start >= 0 then start else len - start
              val normalizedEnd   = if end >= 0 then end else len - end
              if normalizedStart >= normalizedEnd then Right(ScadisString(""))
              else
                Right(
                  ScadisString(
                    s.substring(
                      Math.min(normalizedStart, len - 1),
                      Math.min(normalizedEnd, len - 1)
                    )
                  )
                )
            }
            case _ => Left(StoreError(Constants.WRONGTYPE))
          }
        case None => Right(ScadisString(""))
      }
    }
  }

  override def incr(key: K): F[StoreOperationResult[Int]] = incrby(key, 1)

  override def incrby(key: K, increment: Int): F[StoreOperationResult[Int]] = state.modify { map =>
    map.get(key) match {
      case Some(value) => {
        value match {
          case ScadisString(s) => {
            Try(s.toInt) match {
              case Success(intValue) => {
                val newValue = intValue + increment
                (map.updated(key, ScadisString(newValue.toString)), Right(newValue))
              }
              case _ => (map, Left(StoreError(Constants.INVALID_INT)))
            }
          }
          case _ => (map, Left(StoreError(Constants.INVALID_INT)))
        }
      }
      case None => (map.updated(key, ScadisString(increment.toString)), Right(increment))
    }
  }

  override def strlen(key: K): F[StoreOperationResult[Int]] = state.get.map { map =>
    map
      .get(key)
      .map {
        case ScadisString(s) => Right(s.length)
        case _               => Left(StoreError(Constants.WRONGTYPE))
      }
      .getOrElse(Right(0))
  }
}
