package scadis.server.store

import cats.Monad
import cats.effect.Concurrent
import cats.effect.Ref
import cats.effect.std.Console
import cats.syntax.all.*
import scadis.server.*
import scadis.server.ScadisDataType.*

import scala.util.{Success, Try}

case class StoreError(error: String)

type StoreOperationResult[ResultType] = Either[StoreError, ResultType]

trait Store[F[_], K, V] extends GenericOps[F, K, V] with StringOps[F, K, V] with ListOps[F, K, V] {}

private class RefStoreImpl[F[_]: Console: Monad, K](
    override val state: Ref[F, Map[K, ScadisDataType]]
) extends RefStore[F, K, ScadisDataType]
    with GenericOpsImpl[F, K]
    with StringOpsImpl[F, K]
    with ListOpsImpl[F, K]
    with Store[F, K, ScadisDataType]

object Store {
  def of[F[_]: Concurrent: Console, K]: F[Store[F, K, ScadisDataType]] = Ref
    .of[F, Map[K, ScadisDataType]](Map.empty)
    .map(ref => new RefStoreImpl[F, K](ref))
}
