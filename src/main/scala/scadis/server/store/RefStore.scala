package scadis.server.store

import cats.effect.Ref

abstract class RefStore[F[_], K, V] {
  def state: Ref[F, Map[K, V]]
}
