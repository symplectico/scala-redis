package scadis.server

import scadis.protocol.ScadisProtocolType
import scadis.protocol.ScadisProtocolType.BulkString
import scadis.server.GenericCommands.*
import scadis.server.StringCommands.*
import scadis.server.ListCommands.*

import scala.util.{Failure, Success, Try}

trait CommandParser[K] {
  def parse(input: ScadisProtocolType.SArray): CommandParserResult[Command[K]]
}

case class CommandParserError(error: String)

type CommandParserResult[CommandType] = Either[CommandParserError, CommandType]

private class CommandParserImpl extends CommandParser[String] {
  def parseCopy(args: Vector[String]): CommandParserResult[COPY[String]] = {
    if args.size < 2 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else if args.size > 2 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(COPY[String](args.head, args.tail.head))
  }

  def parseDel(args: Vector[String]): CommandParserResult[DEL[String]] = {
    if args.isEmpty then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(DEL[String](args))
  }

  def parseExists(args: Vector[String]): CommandParserResult[EXISTS[String]] = {
    if args.isEmpty then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(EXISTS[String](args))
  }

  def parseType(args: Vector[String]): CommandParserResult[TYPE[String]] = {
    if args.size != 1 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(TYPE[String](args.head))
  }

  def parseAppend(args: Vector[String]): CommandParserResult[APPEND[String, String]] = {
    if args.size != 2 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(APPEND(args.head, args.tail.head))
  }

  def parseDecr(args: Vector[String]): CommandParserResult[DECR[String]] = {
    if args.size != 1 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(DECR[String](args.head))
  }

  def parseDecrBy(args: Vector[String]): CommandParserResult[DECRBY[String]] = {
    if args.size != 2 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else
      Try(args.tail.head.toInt).map { i =>
        DECRBY[String](args.head, i)
      }.toEither.left.map(_ => CommandParserError(Constants.INVALID_INT))
  }

  def parseGet(args: Vector[String]): CommandParserResult[GET[String]] = {
    if args.size != 1 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(GET[String](args.head))
  }

  def parseGetDel(args: Vector[String]): CommandParserResult[GETDEL[String]] = {
    if args.size != 1 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(GETDEL[String](args.head))
  }

  def parseGetRange(args: Vector[String]): CommandParserResult[GETRANGE[String]] = {
    args match
      case Vector(key, start, end) =>
        (for {
          startInt <- Try(start.toInt)
          endInt   <- Try(end.toInt)
        } yield GETRANGE[String](key, startInt, endInt)).toEither.left.map(_ =>
          CommandParserError(Constants.INVALID_INT)
        )
      case _ =>
        Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseGetSet(args: Vector[String]): CommandParserResult[GETSET[String]] = {
    args match
      case Vector(key, value) => Right(GETSET(key, value))
      case _                  => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseIncr(args: Vector[String]): CommandParserResult[INCR[String]] = {
    args match
      case Vector(key) => Right(INCR(key))
      case _           => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseIncrBy(args: Vector[String]): CommandParserResult[INCRBY[String]] = {
    args match
      case Vector(key, increment) =>
        (for {
          incrementInt <- Try(increment.toInt)
        } yield INCRBY(key, incrementInt)).toEither.left.map(_ =>
          CommandParserError(Constants.INVALID_INT)
        )
      case _ => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseMGet(args: Vector[String]): CommandParserResult[MGET[String]] = {
    Right(MGET(args))
  }

  def parseMSet(args: Vector[String]): CommandParserResult[MSET[String]] = {
    if args.size % 2 != 0 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(MSET(args.grouped(2).map(group => (group(0), group(1)))))
  }

  def parseSet(args: Vector[String]): CommandParserResult[SET[String]] = {
    args match
      case Vector(key, value) => Right(SET(key, value))
      case _                  => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseStrlen(args: Vector[String]): CommandParserResult[STRLEN[String]] = {
    args match
      case Vector(key) => Right(STRLEN(key))
      case _           => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseLindex(args: Vector[String]): CommandParserResult[LINDEX[String]] = {
    args match
      case Vector(key, index) =>
        (for {
          indexInt <- Try(index.toInt)
        } yield LINDEX(key, indexInt)).toEither.left.map(_ =>
          CommandParserError(Constants.INVALID_INT)
        )
      case _ => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseLinsert(args: Vector[String]): CommandParserResult[LINSERT[String]] = {
    val positionValues = List(InsertPosition.BEFORE.toString, InsertPosition.AFTER.toString)

    if args.size != 4 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else if !positionValues.contains(args(1)) then {
      Left(CommandParserError(Constants.SYNTAX_ERROR))
    } else {
      Right(LINSERT(args(0), args(2), args(3), InsertPosition.valueOf(args(1))))
    }
  }

  def parseLlen(args: Vector[String]): CommandParserResult[LLEN[String]] = {
    if args.size != 1 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else Right(LLEN[String](args.head))
  }

  def parseLmove(args: Vector[String]): CommandParserResult[LMOVE[String]] = {
    val sideValues = List(Side.LEFT.toString, Side.RIGHT.toString)
    if args.size != 4 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else if sideValues.contains(args(2)) && sideValues.contains(args(3)) then {
      Right(LMOVE(args(0), args(1), Side.valueOf(args(2)), Side.valueOf(args(3))))
    } else {
      Left(CommandParserError(Constants.SYNTAX_ERROR))
    }
  }

  def parseLmpop(args: Vector[String]): CommandParserResult[LMPOP[String]] = {
    val sideValues = List(Side.LEFT, Side.RIGHT)

    if args.size < 4 then Left(CommandParserError(Constants.ERR_ARG_NUM))
    else {
      if args(args.length - 2) == Constants.COUNT then {
        val side = args(args.length - 3)
        if sideValues.contains(side) then {
          (for {
            numKeys  <- Try(args(0).toInt)
            countInt <- Try(args.last.toInt)
          } yield LMPOP(
            numKeys,
            args.slice(1, args.length - 3).toList,
            Side.valueOf(side),
            Some(countInt)
          )).toEither.left.map(_ => CommandParserError(Constants.INVALID_INT))
        } else {
          Left(CommandParserError(Constants.SYNTAX_ERROR))
        }
      } else if sideValues.contains(args.last) then {
        (for {
          numKeys <- Try(args(0).toInt)
        } yield LMPOP(
          numKeys,
          args.slice(1, args.length - 1).toList,
          Side.valueOf(args.last),
          None
        )).toEither.left.map(_ => CommandParserError(Constants.INVALID_INT))
      } else {
        Left(CommandParserError(Constants.SYNTAX_ERROR))
      }
    }
  }

  def parseLpop(args: Vector[String]): CommandParserResult[LPOP[String]] = {
    args match
      case Vector(key) => Right(LPOP(key, None))
      case Vector(key, count) =>
        (for {
          countInt <- Try(count.toInt)
        } yield LPOP(key, Some(countInt))).toEither.left.map(_ =>
          CommandParserError(Constants.INVALID_INT)
        )
      case _ => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseLpos(args: Vector[String]): CommandParserResult[LPOS[String]] = {
    args match
      case Vector(key, element) => Right(LPOS(key, element, 0, 0, 0)) // TODO
      case _                    => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseLpush(args: Vector[String]): CommandParserResult[LPUSH[String]] = {
    args match
      case Vector(key, elements*) => Right(LPUSH(key, elements.toList))
      case _                      => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseLpushx(args: Vector[String]): CommandParserResult[LPUSHX[String]] = {
    args match
      case Vector(key, elements*) => Right(LPUSHX(key, elements.toList))
      case _                      => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseLrange(args: Vector[String]): CommandParserResult[LRANGE[String]] = {
    args match
      case Vector(key, start, stop) =>
        (for {
          startInt <- Try(start.toInt)
          stopInt  <- Try(stop.toInt)
        } yield LRANGE(key, startInt, stopInt)).toEither.left.map(_ =>
          CommandParserError(Constants.INVALID_INT)
        )
      case _ => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseLrem(args: Vector[String]): CommandParserResult[LREM[String]] = {
    args match
      case Vector(key, count, element) =>
        (for {
          countInt <- Try(count.toInt)
        } yield LREM(key, countInt, element)).toEither.left.map(_ =>
          CommandParserError(Constants.INVALID_INT)
        )
      case _ => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseLset(args: Vector[String]): CommandParserResult[LSET[String]] = {
    args match
      case Vector(key, index, element) =>
        (for {
          indexInt <- Try(index.toInt)
        } yield LSET(key, indexInt, element)).toEither.left.map(_ =>
          CommandParserError(Constants.INVALID_INT)
        )
      case _ => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseLtrim(args: Vector[String]): CommandParserResult[LTRIM[String]] = {
    args match
      case Vector(key, start, stop) =>
        (for {
          startInt <- Try(start.toInt)
          stopInt  <- Try(stop.toInt)
        } yield LTRIM(key, startInt, stopInt)).toEither.left.map(_ =>
          CommandParserError(Constants.INVALID_INT)
        )
      case _ => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseRpop(args: Vector[String]): CommandParserResult[RPOP[String]] = {
    args match
      case Vector(key) => Right(RPOP(key, None))
      case Vector(key, count) =>
        (for {
          countInt <- Try(count.toInt)
        } yield RPOP(key, Some(countInt))).toEither.left.map(_ =>
          CommandParserError(Constants.INVALID_INT)
        )
      case _ => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseRpush(args: Vector[String]): CommandParserResult[RPUSH[String]] = {
    args match
      case Vector(key, elements*) => Right(RPUSH(key, elements.toList))
      case _                      => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def parseRpushx(args: Vector[String]): CommandParserResult[RPUSHX[String]] = {
    args match
      case Vector(key, elements*) => Right(RPUSHX(key, elements.toList))
      case _                      => Left(CommandParserError(Constants.ERR_ARG_NUM))
  }

  def unpackRedisArray(input: ScadisProtocolType.SArray): Option[Vector[String]] =
    input.vals.map {
      case BulkString(s) => Some(s)
      case _             => None
    }.foldLeft(Option(Vector.empty[String])) { (vec, optVal) =>
      optVal match
        case Some(v) => vec.map(_.appended(v))
        case None    => None
    }

  override def parse(input: ScadisProtocolType.SArray): CommandParserResult[Command[String]] =
    unpackRedisArray(input) match
      case Some(cmd +: args) =>
        cmd match
          case "COPY"   => parseCopy(args)
          case "DEL"    => parseDel(args)
          case "EXISTS" => parseExists(args)
          case "TYPE"   => parseType(args)

          case "APPEND"   => parseAppend(args)
          case "DECR"     => parseDecr(args)
          case "DECRBY"   => parseDecrBy(args)
          case "GET"      => parseGet(args)
          case "GETDEL"   => parseGetDel(args)
          case "GETRANGE" => parseGetRange(args)
          case "GETSET"   => parseGetSet(args)
          case "INCR"     => parseIncr(args)
          case "INCRBY"   => parseIncrBy(args)
          case "MGET"     => parseMGet(args)
          case "MSET"     => parseMSet(args)
          case "SET"      => parseSet(args)
          case "STRLEN"   => parseStrlen(args)

          case "LINDEX"  => parseLindex(args)
          case "LINSERT" => parseLinsert(args)
          case "LLEN"    => parseLlen(args)
          case "LMOVE"   => parseLmove(args)
          case "LMPOP"   => parseLmpop(args)
          case "LPOP"    => parseLpop(args)
          case "LPOS"    => parseLpos(args)
          case "LPUSH"   => parseLpush(args)
          case "LPUSHX"  => parseLpushx(args)
          case "LRANGE"  => parseLrange(args)
          case "LREM"    => parseLrem(args)
          case "LSET"    => parseLset(args)
          case "LTRIM"   => parseLtrim(args)
          case "RPOP"    => parseRpop(args)
          case "RPUSH"   => parseRpush(args)
          case "RPUSHX"  => parseRpushx(args)

          case _ => Left(CommandParserError(s"${Constants.UNKNOWN_CMD} '$cmd'"))

      case _ => Left(CommandParserError("invalid data type"))
}

object CommandParser {
  def of: CommandParser[String] = new CommandParserImpl
}
