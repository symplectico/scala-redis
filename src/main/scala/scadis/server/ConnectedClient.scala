package scadis.server

import java.util.UUID
import com.comcast.ip4s.SocketAddress
import com.comcast.ip4s.IpAddress
import java.time.LocalDateTime

case class ConnectedClient(
    id: UUID,
    addr: SocketAddress[IpAddress],
    laddr: SocketAddress[IpAddress],
    name: Option[String],
    createdAt: LocalDateTime,
    lastCommandAt: Option[LocalDateTime],
    cmd: Option[String]
)
