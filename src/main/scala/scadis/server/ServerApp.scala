package scadis.server

import cats.effect.{ExitCode, IO, IOApp}
import com.comcast.ip4s.Port

object ServerApp extends IOApp {
  override def run(args: List[String]): IO[ExitCode] =
    Port.fromInt(5555) match {
      case Some(port) =>
        Server.start[IO](port).compile.drain.as(ExitCode.Success)
    }
}
