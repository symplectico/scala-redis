package scadis.server

import cats.{Functor, Monad}
import cats.syntax.all.*
import scadis.server.GenericCommands.*
import scadis.server.StringCommands.*
import scadis.server.ListCommands.*
import scadis.protocol.*
import scadis.protocol.ScadisProtocolType.*
import scadis.server.ScadisDataType.{ScadisHashMap, ScadisList, ScadisString}
import scadis.server.store.{Store, StoreError, StoreOperationResult}

import scala.util.{Success, Try}

trait CommandInterpreter[F[_], K, V, O] {
  def eval(cmd: Command[K]): F[O]
}

private class CommandInterpreterImpl[F[_]: Monad](
    store: Store[F, String, ScadisDataType]
) extends CommandInterpreter[F, String, ScadisDataType, ScadisProtocolType] {
  override def eval(cmd: Command[String]): F[ScadisProtocolType] = cmd match {
    case COPY(sourceKey, destinationKey) =>
      store
        .copy(sourceKey, destinationKey)
        .map(success => SInteger(if success then 1 else 0))
    case DEL(keys)    => store.del(keys).map(SInteger(_))
    case EXISTS(keys) => store.exists(keys).map(SInteger(_))
    case TYPE(key)    => store.typeOp(key).map(BulkString(_))

    case APPEND(key, value)     => store.append(key, ScadisString(value)).map(convertStoreResult)
    case DECR(key)              => store.decr(key).map(convertStoreResult)
    case DECRBY(key, decrement) => store.decrby(key, decrement).map(convertStoreResult)
    case GET(key) =>
      store.get(key).map {
        case Right(value) =>
          value match
            case Some(v) =>
              v match
                case ScadisString(s) => BulkString(s)
                case _               => SError(Constants.WRONGTYPE)
            case None => Nil
        case Left(err) => SError(err.error)
      }
    case GETDEL(key) =>
      store.getdel(key).map {
        case Right(value) =>
          value match
            case Some(v) =>
              v match
                case ScadisString(s) => BulkString(s)
                case _               => SError(Constants.WRONGTYPE)
            case None => Nil
        case Left(err) => SError(err.error)
      }
    case GETRANGE(key, start, end) => store.getrange(key, start, end).map(convertStoreResult)
    case GETSET(key, value) =>
      store.getset(key, ScadisString(value)).map {
        case Some(v) =>
          v match
            case ScadisString(s) => BulkString(s)
            case _               => BulkString("-1")
        case None => BulkString("-1")
      }
    case INCR(key)              => store.incr(key).map(convertStoreResult)
    case INCRBY(key, increment) => store.incrby(key, increment).map(convertStoreResult)
    case MGET(keys) =>
      store
        .mget(keys)
        .map(vals => {
          SArray(
            vals.iterator
              .to(Vector)
              .foldLeft(Vector.empty) {
                case (result, ScadisString(s)) => BulkString(s) +: result
                case (result, _)               => result
              }
          )
        })
    case MSET(keyVals) =>
      store
        .mset(keyVals.iterator.map { case (k, v) => (k, ScadisString(v)) })
        .map(_ => SimpleString("OK"))
    case SET(key, value) => store.set(key, ScadisString(value)).map(_ => SimpleString("OK"))
    case STRLEN(key) =>
      store
        .strlen(key)
        .map {
          case Right(len)       => BulkString(len.toString)
          case Left(storeError) => SError(storeError.error)
        }

    case LINDEX(key, index) =>
      store.lindex(key, index).map {
        case Right(Some(ScadisString(value))) => BulkString(value)
        case Right(None)                      => Nil
        case Right(_)                         => SError("error")
        case Left(error)                      => SError(error.toString)
      }
    case LINSERT(key, pivot, element, pos) =>
      store
        .linsert(key, pos == InsertPosition.BEFORE, ScadisString(pivot), ScadisString(element))
        .map {
          case Right(n)    => SInteger(n)
          case Left(error) => SError(error.toString)
        }
    case LLEN(key) =>
      store.llen(key).map {
        case Right(n)    => SInteger(n)
        case Left(error) => SError(error.toString)
      }
    case LMOVE(source, destination, from, to) =>
      store.lmove(source, destination, from == Side.LEFT, to == Side.LEFT).map {
        case Right(Some(ScadisString(value))) => BulkString(value)
        case Right(None)                      => Nil
        case Right(_)                         => SError("error")
        case Left(error)                      => SError(error.toString)
      }
    case LMPOP(numKeys, keys, side, count) =>
      store.lmpop(numKeys, keys, side == Side.LEFT, count).map {
        case Right(ScadisList(elements)) if elements.nonEmpty =>
          SArray(elements.map(BulkString.apply))
        case Right(ScadisList(elements)) if elements.isEmpty => Nil
        case Right(_)                                        => SError("error")
        case Left(error)                                     => SError(error.toString)
      }
    case LPOP(key, count) =>
      store.lpop(key, count).map {
        case Right(ScadisString(value)) => BulkString(value)
        case Right(_)                   => SError("error")
        case Left(error)                => SError(error.toString)
      }
    case LPOS(key, element, rank, numMatches, maxLen) =>
      store.lpos(key, ScadisString(element)).map {
        case Right(Some(idx)) => SInteger(idx)
        case Right(None)      => Nil
        case Left(error)      => SError(error.toString)
      }
    case LPUSH(key, values) =>
      store.lpush(key, values.map(ScadisString.apply)).map {
        case Right(n)    => SInteger(n)
        case Left(error) => SError(error.toString)
      }
    case LPUSHX(key, values) =>
      store.lpushx(key, values.map(ScadisString.apply)).map {
        case Right(n)    => SInteger(n)
        case Left(error) => SError(error.toString)
      }
    case LRANGE(key, start, stop) =>
      store.lrange(key, start, stop).map {
        case Right(ScadisList(elements)) => SArray(elements.map(BulkString.apply))
        case Right(_)                    => SError("error")
        case Left(error)                 => SError(error.toString)
      }
    case LREM(key, count, element) =>
      store.lrem(key, count, ScadisString(element)).map {
        case Right(n)    => SInteger(n)
        case Left(error) => SError(error.toString)
      }
    case LSET(key, index, element) =>
      store.lset(key, index, ScadisString(element)).map {
        case Right(_)    => SimpleString(Constants.OK)
        case Left(error) => SError(error.toString)
      }
    case LTRIM(key, start, stop) =>
      store.ltrim(key, start, stop).map {
        case Right(_)    => SimpleString(Constants.OK)
        case Left(error) => SError(error.toString)
      }
    case RPOP(key, count) =>
      store.rpop(key, count).map {
        case Right(Some(ScadisString(value))) => BulkString(value)
        case Right(Some(ScadisList(values)))  => SArray(values.map(BulkString.apply))
        case Right(None)                      => Nil
        case Right(_)                         => SError("error")
        case Left(error)                      => SError(error.toString)
      }
    case RPUSH(key, values) =>
      store.rpush(key, values.map(ScadisString.apply)).map {
        case Right(n)    => SInteger(n)
        case Left(error) => SError(error.toString)
      }
    case RPUSHX(key, values) =>
      store.rpush(key, values.map(ScadisString.apply)).map {
        case Right(n)    => SInteger(n)
        case Left(error) => SError(error.toString)
      }
  }

  private def convertStoreResult[T](result: StoreOperationResult[T]): ScadisProtocolType =
    result match
      case Right(value) => BulkString(value.toString)
      case Left(error)  => SError(error.error)
}

object CommandInterpreter {
  def of[F[_]: Monad](
      store: Store[F, String, ScadisDataType]
  ): CommandInterpreter[F, String, ScadisDataType, ScadisProtocolType] =
    new CommandInterpreterImpl[F](store)
}
