package scadis.server

object Constants {
  val OK = "OK"

  val WRONGTYPE    = "WRONGTYPE Operation against a key holding the wrong type of value"
  val INVALID_INT  = "value it not an integer or out of range"
  val ERR_ARG_NUM  = "ERR wrong number of arguments for command"
  val UNKNOWN_CMD  = "unknown command"
  val SYNTAX_ERROR = "syntax error"

  val TYPE_STRING   = "string"
  val TYPE_LIST     = "list"
  val TYPE_SET      = "set"
  val TYPE_HASH_MAP = "hash"
  val TYPE_NONE     = "none"

  val COUNT = "COUNT"
}
