package scadis.server

import cats.effect.kernel.Resource
import cats.{Applicative, FlatMap, MonadError}
import cats.effect.std.Console
import cats.effect.{Async, Concurrent, IO, Ref}
import cats.implicits.*
import com.comcast.ip4s.{IpAddress, Port, SocketAddress}
import fs2.{Chunk, INothing, RaiseThrowable, Stream}
import fs2.io.net.{Network, Socket}
import fs2.interop.scodec.{StreamDecoder, StreamEncoder}
import scadis.protocol.{Codecs, ScadisProtocolType}
import scadis.protocol.ScadisProtocolType.*
import scadis.server.StringCommands.{GET, SET}
import scadis.server.store.Store
import scodec.bits.BitVector

import java.time.LocalDateTime
import java.util.UUID
import java.util.concurrent.ThreadLocalRandom
import scala.util.{Failure, Success, Try}

object Server {
  private val commandParser: CommandParser[String] = CommandParser.of

  def start[F[_]: Async: Network: Console](port: Port): Stream[F, INothing] = {

    val connectedClientsStream = Stream.eval(Ref[F].of(List[ConnectedClient]()))
    val store                  = Store.of[F, String]
    val commandInterpreter     = Stream.eval(store.map(CommandInterpreter.of[F](_)))

    Stream.exec(Console[F].println(s"Starting server on $port")) ++
      connectedClientsStream.zip(commandInterpreter).flatMap { (clientsRef, commandInterpreter) =>
        Network[F]
          .server(port = Some(port))
          .map(handleClient(_, clientsRef, commandInterpreter))
          .parJoinUnbounded
      }
  }

  def handleClient[F[_]: Concurrent: Console](
      clientSocket: Socket[F],
      clientsRef: Ref[F, List[ConnectedClient]],
      commandInterpreter: CommandInterpreter[F, String, ScadisDataType, ScadisProtocolType]
  ): Stream[F, INothing] = {
    val newConnectedClient =
      for {
        addr  <- clientSocket.remoteAddress
        laddr <- clientSocket.localAddress
      } yield createConnectedClient(addr, laddr)

    Stream.exec(
      for {
        newClient <- newConnectedClient
        _         <- Console[F].println(s"Registering new client $newClient")
        clients   <- clientsRef.updateAndGet(_.appended(newClient))
        _         <- Console[F].println(s"Clients are $clients")
      } yield ()
    ) ++
      processClient(
        clientSocket,
        commandInterpreter
      )
  }

  def createConnectedClient(
      remoteAddress: SocketAddress[IpAddress],
      localAddress: SocketAddress[IpAddress]
  ): ConnectedClient = {
    ConnectedClient(
      UUID.randomUUID(),
      remoteAddress,
      localAddress,
      None,
      LocalDateTime.now(),
      None,
      None
    )
  }

  def processClient[F[_]: Concurrent: Console](
      clientSocket: Socket[F],
      commandInterpreter: CommandInterpreter[F, String, ScadisDataType, ScadisProtocolType]
  ): Stream[F, INothing] = {
    clientSocket.reads
      .through(StreamDecoder.tryMany(Codecs.array).toPipeByte)
      .flatTap(x => Stream.eval(Console[F].println(x)))
      .map(commandParser.parse)
      .flatTap(x => Stream.eval(Console[F].println(x)))
      .flatMap(interpret(_, clientSocket, commandInterpreter))
      .drain
  }

  def interpret[F[_]: Console: Concurrent](
      cmdParserResult: CommandParserResult[Command[String]],
      clientSocket: Socket[F],
      commandInterpreter: CommandInterpreter[F, String, ScadisDataType, ScadisProtocolType]
  ): Stream[F, Unit] = {
    Stream.eval {
      cmdParserResult.map(commandInterpreter.eval) match
        case Right(value) => value
        case Left(e)      => Concurrent[F].pure(SError(e.toString))
    }.through(StreamEncoder.many(Codecs.redisType).toPipeByte)
      .through(clientSocket.writes)
  }
}
