package scadis.server

enum ScadisDataType {
  case ScadisString(string: String)
  case ScadisList(elements: Vector[String])
  case ScadisSet(elements: Set[String])
  case ScadisHashMap(contents: Map[String, String])
}
