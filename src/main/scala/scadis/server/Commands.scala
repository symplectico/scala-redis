package scadis.server

sealed trait Command[K]

object GenericCommands {
  case class COPY[K](sourceKey: K, destinationKey: K) extends Command[K]

  case class DEL[K](keys: IterableOnce[K]) extends Command[K]

  case class EXISTS[K](keys: IterableOnce[K]) extends Command[K]

  case class TYPE[K](key: K) extends Command[K]
}

object StringCommands {
  case class APPEND[K, V](key: K, value: K) extends Command[K]

  case class DECR[K](key: K) extends Command[K]

  case class DECRBY[K](key: K, decrement: Int) extends Command[K]

  case class GET[K](key: K) extends Command[K]

  case class GETDEL[K](key: K) extends Command[K]

  case class GETRANGE[K](key: K, start: Int, end: Integer) extends Command[K]

  case class GETSET[K](key: K, value: K) extends Command[K]

  case class INCR[K](key: K) extends Command[K]

  case class INCRBY[K](key: K, increment: Int) extends Command[K]

  case class MGET[K](keys: IterableOnce[K]) extends Command[K]

  case class MSET[K](keyVals: IterableOnce[(K, K)]) extends Command[K]

  case class SET[K](key: K, value: K) extends Command[K]

  case class STRLEN[K](key: K) extends Command[K]
}

object ListCommands {
  enum InsertPosition {
    case BEFORE
    case AFTER
  }

  enum Side {
    case LEFT
    case RIGHT
  }

  case class LINDEX[K](key: K, index: Int) extends Command[K]

  case class LINSERT[K](key: K, pivot: K, element: K, pos: InsertPosition) extends Command[K]

  case class LLEN[K](key: K) extends Command[K]

  case class LMOVE[K](source: K, destination: K, from: Side, to: Side) extends Command[K]

  case class LMPOP[K](
      numKeys: Int,
      keys: List[K],
      side: Side,
      count: Option[Int]
  ) extends Command[K]

  case class LPOP[K](key: K, count: Option[Int]) extends Command[K]

  case class LPOS[K](
      key: K,
      element: K,
      rank: Int,
      numMatches: Int,
      maxLen: Int
  ) extends Command[K]

  case class LPUSH[K](key: K, values: List[K]) extends Command[K]

  case class LPUSHX[K](key: K, values: List[K]) extends Command[K]

  case class LRANGE[K](key: K, start: Int, stop: Int) extends Command[K]

  case class LREM[K](key: K, count: Int, element: K) extends Command[K]

  case class LSET[K](key: K, index: Int, element: K) extends Command[K]

  case class LTRIM[K](key: K, start: Int, stop: Int) extends Command[K]

  case class RPOP[K](key: K, count: Option[Int]) extends Command[K]

  case class RPUSH[K](key: K, values: List[K]) extends Command[K]

  case class RPUSHX[K](key: K, values: List[K]) extends Command[K]
}
