# Scala Redis

This is a toy version of [Redis](https://redis.io/) that I've written to practice using the [Scala](https://www.scala-lang.org/) [Typelevel stack](https://typelevel.org/projects/).

The communication over the network uses the [Redis binary protocol](https://redis.io/docs/reference/protocol-spec/).

## Libraries
- [cats](https://github.com/typelevel/cats)
- [cats-effect](https://github.com/typelevel/cats-effect/)
- [fs2](https://github.com/typelevel/fs2)
- [scodec](https://github.com/scodec/scodec)

## Supported Commands
The behavior of these commands is described [here](https://redis.io/commands/). In some cases not the full set of options that Redis supports is implemented.

- DEL
- EXISTS
- TYPE
- APPEND
- DECR
- DECRBY
- GET
- GETDEL
- GETRANGE
- GETSET
- INCR
- INCRBY
- MGET
- MSET
- SET
- STRLEN
- LINDEX
- LINSERT
- LLEN
- LMOVE
- LMPOP
- LPOP
- LPOS
- LPUSH
- LPUSHX
- LRANGE
- LREM
- LSET
- LTRIM
- RPOP
- RPUSH
- RPUSHX