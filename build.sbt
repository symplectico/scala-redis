name := "scadis"

scalaVersion := "3.3.0"

libraryDependencies ++= Seq(
  "co.fs2" %% "fs2-io" % "3.7.0",
  "co.fs2" %% "fs2-scodec" % "3.7.0",
  "org.slf4j" % "slf4j-simple" % "2.0.5",
  "org.jline" % "jline" % "3.23.0",
  "com.monovore" %% "decline" % "2.4.1",
  "com.disneystreaming" %% "weaver-cats" % "0.8.3" % Test
)

testFrameworks += new TestFramework("weaver.framework.CatsEffect")

run / fork := true
outputStrategy := Some(StdoutOutput)
run / connectInput := true

scalafmtOnCompile := true

enablePlugins(UniversalPlugin, JavaAppPackaging)
